// All api url
const getDebtsCountURL = 'http://rekrutacja-webhosting.it.krd.pl/api/Recruitment/GetDebtsCount';
const getTopDebtsURL = 'http://rekrutacja-webhosting.it.krd.pl/api/Recruitment/GetTopDebts';
const postFilteredDebtsURL = 'http://rekrutacja-webhosting.it.krd.pl/api/Recruitment/GetFilteredDebts';

export {
    getDebtsCountURL,
    getTopDebtsURL,
    postFilteredDebtsURL
};
