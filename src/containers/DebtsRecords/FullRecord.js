import React from 'react';
import ShortRecord from './ShortRecord';

const FullRecord = (props) => {
        //Get props after spred
        const {
            secondLabel,
            secondValue
        } = props;

        //This component render full row wit detail of record
        return (
            <td className="debts__record debts__record--full">

                <ShortRecord {...props} btnText="mniej" />

                <div className="debts__data">
                    <div className="debts__text debts__text--tiny debts__text--grey">
                        {secondLabel}
                    </div>
                    <div className="debts__text debts__text--large debts__text--blue">
                        {secondValue}
                    </div>
                </div>
            </td>
        )
    }


export default FullRecord;
