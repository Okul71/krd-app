import React from 'react';

const ShortRecord = (props) => {
    const {
        id,
        firstValue,
        setActiveRecord,
        btnText
    } = props;

    //Set button name
    const isbtnText= btnText ? btnText : 'wiecej';

    if(!setActiveRecord) {
        return(
            <div className="debts__tableRow">
                <div className="debts__text debts__text--large debts__text--blue">
                    {firstValue}
                </div>
            </div>
        );
    }

    return(
        <div className="debts__tableRow">
            <button
                className="debts__button debts__button--text"
                onClick={() => setActiveRecord(id)}
            >
                {isbtnText}
            </button>
        </div>
    );

}

export default ShortRecord;
