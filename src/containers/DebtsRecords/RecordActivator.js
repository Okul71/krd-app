import React from 'react';
import FullRecord from './FullRecord';
import ShortRecord from './ShortRecord';

// Helper function for show and hiddeing data from row
const isActive = (id, activeRecord) => activeRecord === id ? true: false;

// This component need refactor, meybe move his method to index, I need to think
const RecordActivator = (props) => {
    const {
        id,
        activeRecord,
    } = props;

    //If is no active render ShortRecord
    if(!isActive(id, activeRecord)) {
        return (<td> <ShortRecord {...props} /> </td>);
    }

    return (
        <FullRecord {...props} /> // Good
    );


};

export default RecordActivator;
