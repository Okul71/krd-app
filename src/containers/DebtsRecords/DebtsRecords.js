import React from 'react';
import RecordActivator from './RecordActivator';

//DebtsRecords map iterate over array and render every
//item, also has function for check if some record is active
const DebtsRecords = ({debtsData, activeRecord, setActiveRecord}) => {

    return debtsData.map(debt => {
        return (
            <tr key={debt.Id}>

                <RecordActivator
                    id={debt.Id}
                    firstLabel={'Dłużnik'}
                    firstValue={debt.Name}
                    secondLabel={'Adres'}
                    secondValue={debt.Address}
                    activeRecord={activeRecord}
                />

                <RecordActivator
                        id={debt.Id}
                        firstLabel={'Nip'}
                        firstValue={debt.NIP}
                        secondLabel={'Rodzaj/Typ dokumentu stanowiący podstawę dla wierzytelności'}
                        secondValue={debt.DocumentType}
                        activeRecord={activeRecord}
                    />

                <RecordActivator
                        id={debt.Id}
                        firstLabel={'Kwota zadłużenia'}
                        firstValue={debt.Value}
                        secondLabel={'Cena zadłużenia'}
                        secondValue={debt.Price}
                        activeRecord={activeRecord}
                    />

                <RecordActivator
                        id={debt.Id}
                        secondLabel={'Numer'}
                        secondValue={debt.Number}
                        setActiveRecord={setActiveRecord}
                        activeRecord={activeRecord}
                    />

            </tr>

        );
    });

};

export default DebtsRecords;
