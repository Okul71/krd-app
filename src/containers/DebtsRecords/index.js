import React, {Component} from 'react';
import DebtsRecords from './DebtsRecords';
import TableHeader from './TableHeader';

//DebtList manages state of active debt record
export default class DebtsList extends Component {
    constructor(props) {
        super(props);

        //Initial state
        this.state = {
            activeRecord: null,
        }

        //Methods bind
        this.setActiveRecord = this.setActiveRecord.bind(this);
    }

    //Set active record
    setActiveRecord(id) {
        const {activeRecord} = this.state;

        if(activeRecord !== id) {
            this.setState({
                activeRecord: id
            });
            return;
        }

        this.setState({
            activeRecord: null
        });
    }

    render() {
        const {debtsData} = this.props;
        const {activeRecord} = this.state;

        return(
            <table className="table table-responsive">
                <TableHeader />
                <tbody>

                    <DebtsRecords
                        debtsData={debtsData}
                        activeRecord={activeRecord}
                        setActiveRecord={this.setActiveRecord}
                    />

                </tbody>
            </table>
        );
    }
}
