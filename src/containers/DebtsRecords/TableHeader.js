import React from 'react';

const TableHeader = () => (
    <thead>
        <tr>
            <th className="debts__text debts__text--tiny debts__text--uppercase debts__text--grey">
                Dłużnik
            </th>

            <th className="debts__text debts__text--tiny debts__text--uppercase debts__text--grey">
                Nip
            </th>

            <th colSpan="2"
                className="debts__text debts__text--tiny debts__text--uppercase debts__text--grey">
                Kwota zadłużenia
            </th>

        </tr>
    </thead>
);

export default TableHeader;
