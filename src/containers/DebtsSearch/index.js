import React, {Component} from 'react';

//This component take input value and pass it to function provided by props and
//when user write term and click a button, we send request to server
export default class DebtsSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchTerm: '',
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleChange(e) {
        this.setState({searchTerm: e.target.value});
    }

    handleClick(e) {
        const {postFilteredDebts} = this.props;
        postFilteredDebts(this.state.searchTerm);
    }

    render() {
        return(
            <div>
                <p className="debts__text debts__text--tiny debts__text--white debts__text--uppercase" >
                    Podaj numer sprawy, nazwę lub nip dłużnika
                </p>
                <input className="debts__input" onChange={this.handleChange}/>
                <button
                    className="debts__button debts__button--bg"
                    onClick={this.handleClick}>
                    Szukaj
                </button>
            </div>
        );
    }
}
