import Axios from 'axios';

// Some function to make ajax request easier
export function getAjaxRequest(url, cb) {
    return Axios.get(url)
        .then(function (response) {
            return response.data;
        })
        .catch(function (error) {
            console.log(error);
        });
}

export function postAjaxRequest(url, data) {
    return Axios({
        method: 'post',
        url: url,
        data: data,
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(function (response) {
        return response.data;
    })
    .catch(function (error) {
        console.log(error);
    });
}
