import React from 'react';
import DebtsSearch from '../containers/DebtsSearch';
import DebtsCounter from './DebtsCounter';

const Header = ({debtsCounter, postFilteredDebts}) => (
    <header className="debts__header debts__header--blue">
        <div className="container">
            <div className="row">
                <div className="col-md-6">
                    <DebtsSearch
                        className="debts__search"
                        postFilteredDebts={postFilteredDebts}
                    />
                </div>
                <div className="col-md-6 d-flex justify-content-end">
                    <DebtsCounter
                        className="debts__counter"
                        debtsCounter={debtsCounter} />
                </div>
            </div>
        </div>
    </header>
);

export default Header;
