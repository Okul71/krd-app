import React from 'react';
import DebtsRecords from '../containers/DebtsRecords';

const Section = ({ debtsData }) => (
    <section>
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <DebtsRecords debtsData={debtsData} />
                </div>
            </div>

        </div>
    </section>
);

export default Section;
