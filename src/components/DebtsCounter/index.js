import React from 'react';

const DebtsCounter = ({debtsCounter}) => (
    <div>
        <p className="debts__text debts__text--tiny debts__text--red debts__text--uppercase">
            Całkowita ilość spraw
        </p>
        <div className="debts__text debts__text--larg debts__text--white text-right">{debtsCounter}</div>
    </div>
);

export default DebtsCounter;
