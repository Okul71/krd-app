import React, {Component} from 'react';
import Header from './components/Header';
import Section from './components/Section';
import {getAjaxRequest, postAjaxRequest} from './helper';
import {
    getDebtsCountURL,
    getTopDebtsURL,
    postFilteredDebtsURL
} from './api';

// Main application wrapper. It's provide method to other components
class App extends Component {
    constructor() {
        super();

        // Initial state
        this.state = {
            debtsData: [],
            debtsCounter: null,
        }

        // Bind methods
        this.getDebtsCount = this.getDebtsCount.bind(this);
        this.getTopDebts = this.getTopDebts.bind(this);
        this.postFilteredDebts = this.postFilteredDebts.bind(this);
        this.checkSearchTerm = this.checkSearchTerm.bind(this);
    }

    getDebtsCount() {
        //Save debts counter to our state
        getAjaxRequest(getDebtsCountURL)
            .then(debtsCounter => this.setState({debtsCounter}));

    }

    getTopDebts() {
        //Save debts data to our state
        getAjaxRequest(getTopDebtsURL)
            .then(debtsData => this.setState({debtsData}));
    }

    postFilteredDebts(value) {
        //Prepare entity for server
        const entityToSend = this.checkSearchTerm(value);

        if(!entityToSend) {

            return;
        }

        postAjaxRequest(postFilteredDebtsURL, JSON.stringify(entityToSend))
            .then(debtsData => this.setState({debtsData}));
    }

    checkSearchTerm(value) {
        //Has only numbers
        const nipRegexp = /^[0-9]+$/;
        //Start wit letter DI/
        const numberRegexp = /^DI\//;
        //Has big, small, space and polish letter
        const nameRegexp = /^[\sa-zA-Z-ąćęłńóśźż]+$/;

        //Check what we looking for
        if(nipRegexp.test(value)) {
            return {
                NIP: value
            }
        }

        if(numberRegexp.test(value)) {
            return {
                Number: value
            }
        }

        if(nameRegexp.test(value)) {
            return {
                Name: value
            }
        }

        return false;
    }

    componentDidMount() {
        this.getTopDebts();
        this.getDebtsCount();
    }

    render() {
        const {debtsData, debtsCounter} = this.state;

        return (
            <div className="debts">

                <Header
                    debtsCounter={debtsCounter}
                    postFilteredDebts={this.postFilteredDebts}
                />

                <Section
                    className="debts__content debts__content--white"
                    debtsData={debtsData}
                />

            </div>
        );
    }
}

export default App;
