const gulp = require('gulp');
const less = require('gulp-less');

// Compile less
gulp.task('compile-less', () => {
    gulp.src('./src/less/base.less')
        .pipe(less())
        .pipe(gulp.dest('./public'));
});

// Watch less files for changes
gulp.task('watch-less', () => {
    gulp.watch('./src/**/*.less', ['compile-less']);
});

// Run default task
gulp.task('default', ['watch-less']);
